const mongoose = require('mongoose');

const todoSchema = mongoose.Schema({
  title: String,
  body: String,
  timestamp: String,
}) 

module.exports = mongoose.model('todoSchema', todoSchema);