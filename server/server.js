const express = require('express');
const app = express();
const mongoose = require('mongoose');
const dbTodo = require('./db');
require('dotenv').config();

const port = process.env.PORT;
const db = mongoose.connection;
const API = `api/v1/notes`;

mongoose.set('useFindAndModify', false);

mongoose.connect(process.env.CONNECTION_URL, {
	useCreateIndex: true,
	useNewUrlParser: true,
	useUnifiedTopology: true
});

db.once('open', () => {
  console.log("connected to db");
})

app.use(express.json());
app.use(function (req, res, next) {
	res.setHeader("Access-Control-Allow-Origin", "*");
	res.setHeader("Access-Control-Allow-Methods", "*");
	res.setHeader("Access-Control-Allow-Headers", "*");
	next();
});


// Get all notes
app.get(`/${API}`, (req, res) => {
  dbTodo.find((err, data) => {
    if (err) {
      res.status(500).send(err);
    } else {
      res.status(200).send(data)
    }
  })
});

// Create new note
app.post(`/${API}/new`, (req, res) => {
  const newNote = req.body;

  dbTodo.create(newNote, (err, data) => {
    if (err) {
      res.status(500).send(err);
    } else {
      res.status(201).send(data);
    }
  })
})

// Delete note
app.delete(`/${API}/delete/:id`, (req, res) => {
  const id = req.params.id;

  dbTodo.deleteOne({ _id: id})
    .then(result => {
      if (result.deletedCount === 0) {
        return res.json('There is no note to delete!')
      }
      res.json('Deleted successfully');
    })
    .catch(err => console.log(err));
});


// Update note
app.put(`/${API}/edit/:id`, (req, res) => {
  const id = req.params.id;

  dbTodo.findOneAndUpdate(
    { _id : id},
    {
      $set: {
        title: req.body.title,
        body: req.body.body
      }
    }
  )
  .then(result => {
    res.status(200).json('Edited!');
  })
  .catch(err => {
    res.status(404).json("Not Found!");
  })
})




app.listen(port, () => {
  console.log(`Listening at localhost:${port}`);
});